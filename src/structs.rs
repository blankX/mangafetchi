use std::io;
use std::fmt;
use serde::Deserialize;
extern crate url;
extern crate serde;
extern crate serde_json;

#[derive(Deserialize, Debug)]
pub struct SearchResult {
    pub id: String,
    pub name: String,
    #[serde(rename(deserialize = "nameunsigned"))]
    pub name_unsigned: String,
    #[serde(rename(deserialize = "lastchapter"))]
    pub last_chapter: String,
    pub image: String,
    pub author: String,
    pub story_link: String
}

#[derive(Debug)]
pub struct Chapter {
    pub chapter_number: String,
    pub chapter_name: Option<String>,
    pub domain: String
}

impl fmt::Display for Chapter {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut text = format!("Chapter {}", self.chapter_number);
        if self.chapter_name.is_some() {
            text.push_str(&format!(": {}", self.chapter_name.as_ref().unwrap()));
        }
        formatter.write_str(&text)
    }
}

pub struct Manga {
    pub id: String,
    pub name: String,
    pub authors: Vec<String>,
    pub status: Option<String>,
    pub last_updated: Option<String>,
    pub genres: Vec<String>,
    pub summary: Option<String>,
    pub chapters: Vec<Chapter>
}

impl fmt::Display for Manga {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut text = format!("ID: {}\nName: {}",
            self.id,
            self.name);
        if self.status.is_some() {
            text.push_str(&format!("\nStatus: {}", self.status.as_ref().unwrap()));
        }
        if self.last_updated.is_some() {
            text.push_str(&format!("\nLast Updated: {}", self.last_updated.as_ref().unwrap()));
        }
        if !self.genres.is_empty() {
            text.push_str(&format!("\nGenres: {}", self.genres.join(", ")));
        }
        if !self.authors.is_empty() {
            text.push_str(&format!("\nAuthors: {}", self.authors.join(", ")));
        }
        if self.summary.is_some() {
            text.push_str(&format!("\nSummary:\n{}", self.summary.as_ref().unwrap()));
        }
        text.push_str("\nChapters:");
        for chapter in &self.chapters {
            text.push_str(&format!("\n- {}", &chapter));
        }
        formatter.write_str(&text)
    }
}

pub struct Redirect {
    pub url: String
}

pub enum MangaOption {
    Manga(Manga),
    Redirect(Redirect),
    DoesNotExist
}

#[derive(Debug)]
pub enum Error {
    Reqwest(reqwest::Error),
    URL(url::ParseError),
    SerdeJSON(serde_json::Error),
    IO(io::Error)
}

impl From<reqwest::Error> for Error {
    #[inline]
    fn from(error: reqwest::Error) -> Error {
        Error::Reqwest(error)
    }
}

impl From<url::ParseError> for Error {
    #[inline]
    fn from(error: url::ParseError) -> Error {
        Error::URL(error)
    }
}

impl From<serde_json::Error> for Error {
    #[inline]
    fn from(error: serde_json::Error) -> Error {
        Error::SerdeJSON(error)
    }
}

impl From<io::Error> for Error {
    #[inline]
    fn from(error: io::Error) -> Error {
        Error::IO(error)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str(&match self {
            Error::Reqwest(err) => format!("reqwest: {}", err),
            Error::URL(err) => format!("url: {}", err),
            Error::SerdeJSON(err) => format!("serde_json: {}", err),
            Error::IO(err) => format!("io: {}", err)
        })
    }
}
