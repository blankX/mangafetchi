mod utils;
mod structs;
mod commands;

extern crate tokio;

fn main() {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(commands::run());
}
