mod view;
mod feed;
mod search;
mod download;

use std::env;
use std::path::Path;
use std::process::exit;

pub async fn run() {
    let mut args = env::args();
    let path = args.next().expect("Cannot get binary path");
    let path = Path::new(&path).file_stem().unwrap().to_str().unwrap();
    let operation = match args.next() {
            Some(operation) => operation,
            None => {
                eprintln!("Missing operation, run `{} help`", path);
                exit(1);
            }
    };
    match operation.as_str() {
        "search" => search::run(args).await,
        "info" | "view" | "show" => view::run(args).await,
        "feed" | "rss" => feed::run(args).await,
        "download" | "dl" => download::run(args).await,
        "help" => println!(r#"Usage: {} search <query>
  or   {} info/view/show <manga id> [manga id]...
  or   {} feed/rss <manga id>
  or   {} download/dl <manga id> [chapters]..."#, path, path, path, path),
        _ => {
            eprintln!("Unknown operation, run `{} help`", path);
            exit(1)
        }
    };
}
