use crate::utils;
use crate::structs;

use std::env;
use std::process::exit;
use tokio::task::JoinHandle;
extern crate tokio;
extern crate reqwest;

pub async fn run(args: env::Args) {
    let ids: Vec<String> = args.collect();
    if ids.len() < 1 {
        eprintln!("Missing manga id(s)");
        exit(1);
    }
    let client = reqwest::Client::new();
    let mut handles: Vec<JoinHandle<(structs::MangaOption, String)>> = Vec::with_capacity(ids.len());
    for id in ids {
        let cloned_client = client.clone();
        handles.push(tokio::spawn(async move {
            (utils::get_manga(cloned_client, &id).await.unwrap(), id)
        }));
    }
    let mut fail = false;
    let mut one_done = false;
    for handle in handles {
        let (manga_info, id) = handle.await.unwrap();
        match manga_info {
            structs::MangaOption::Manga(manga_info) => {
                if one_done {
                    println!("");
                }
                println!("{}", &manga_info);
            },
            structs::MangaOption::Redirect(_) => panic!("Nested redirect"),
            structs::MangaOption::DoesNotExist => {
                if one_done {
                    eprintln!("");
                }
                eprintln!("ID: {}\nError: does not exist", id);
                fail = true;
            }
        };
        one_done = true;
    }
    if fail {
        exit(1);
    }
}
