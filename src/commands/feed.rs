use crate::utils;
use crate::structs;

use std::env;
use std::io::Cursor;
use std::process::exit;
use quick_xml::Writer;
use quick_xml::events::{Event, BytesStart, BytesText, BytesEnd};
extern crate reqwest;

pub async fn run(mut args: env::Args) {
    let manga_id = match args.next() {
        Some(manga_id) => manga_id,
        None => {
            eprintln!("Missing manga id");
            exit(1);
        }
    };
    if args.next().is_some() {
        eprintln!("Specify only one manga id");
        exit(1);
    }
    let mut manga_info = match utils::get_manga(reqwest::Client::new(), &manga_id).await.unwrap() {
        structs::MangaOption::Manga(manga_info) => manga_info,
        structs::MangaOption::Redirect(_) => panic!("Nested redirect"),
        structs::MangaOption::DoesNotExist => {
            eprintln!("ID: {}\nError: does not exist", &manga_id);
            exit(1);
        }
    };
    manga_info.chapters.reverse();
    let mut writer = Writer::new(Cursor::new(Vec::new()));
    {
        let mut elem = BytesStart::owned(b"rss".to_vec(), 3);
        elem.push_attribute(("version", "2.0"));
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesStart::owned(b"channel".to_vec(), 7);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesStart::owned(b"title".to_vec(), 5);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&manga_info.name).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"title".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let elem = BytesStart::owned(b"link".to_vec(), 4);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&format!("https://mangakakalot.com/manga/{}", &manga_id)).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"link".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        if manga_info.summary.is_some() {
            let elem = BytesStart::owned(b"description".to_vec(), 11);
            writer.write_event(Event::Start(elem)).unwrap();

            let elem = BytesText::from_plain_str(&manga_info.summary.unwrap()).into_owned();
            writer.write_event(Event::Text(elem)).unwrap();

            let elem = BytesEnd::owned(b"description".to_vec());
            writer.write_event(Event::End(elem)).unwrap();
        }
    }

    for chapter in manga_info.chapters {
        let link = format!("https://mangakakalot.com/chapter/{}/chapter_{}", &manga_id, chapter.chapter_number);
        let elem = BytesStart::owned(b"item".to_vec(), 4);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesStart::owned(b"title".to_vec(), 5);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&format!("{}", &chapter)).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"title".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let elem = BytesStart::owned(b"link".to_vec(), 4);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&link).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"link".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let mut elem = BytesStart::owned(b"guid".to_vec(), 4);
        elem.push_attribute(("isPermaLink", "true"));
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&link).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"guid".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let elem = BytesEnd::owned(b"item".to_vec());
        writer.write_event(Event::End(elem)).unwrap();
    }

    let elem = BytesEnd::owned(b"channel".to_vec());
    writer.write_event(Event::End(elem)).unwrap();

    let elem = BytesEnd::owned(b"rss".to_vec());
    writer.write_event(Event::End(elem)).unwrap();

    println!("{}", String::from_utf8(writer.into_inner().into_inner()).unwrap());
}
