use crate::utils;

use std::env;
use std::process::exit;
extern crate reqwest;

pub async fn run(args: env::Args) {
    let mut query = String::new();
    for arg in args {
        query.push_str(&format!(" {}", arg));
    }
    let query = query.trim();
    if query.is_empty() {
        eprintln!("Missing search query");
        exit(1);
    }
    let results = utils::search(reqwest::Client::new(), &query).await.unwrap();
    if results.is_empty() {
        eprintln!("No results found");
        exit(1);
    }
    for result in results {
        println!("{}: {}", result.name_unsigned, result.name);
    }
}
